import 'package:lab1_02/lab1_02.dart' as lab1_02;
import 'dart:io';
void main() {
  print('Please input sentence :');
  String string = stdin.readLineSync()!.toLowerCase();
  List list = string.split(' ');
  var countList = list.toSet().toList();
  print('The sentence without duplicate word is :');
  var stringList = countList.join(' ');
  print(stringList);
  print('Total of word in this sentence is :');
  print(countList.length);
  var countWord = Map();
  for(var element in list) {
    if(!countWord.containsKey(element)) {
      countWord[element] = 1;
    } else {
      countWord[element] += 1;
    }
  }
  print('Total of each word in this sentence is :');
  print(countWord);
}